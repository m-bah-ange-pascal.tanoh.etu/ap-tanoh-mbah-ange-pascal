import random
def liste_alea(n:int):
    """ construit une liste de longueur n contenant les entiers 0 à n-1 mélangés.

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    liste = []
    for i in range(n):
        random.shuffle(liste)
    return liste

import matplotlib.pyplot as plt
