import timeit
import matplotlib.pyplot as plt
import random

def liste_alea(taille):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    return[random.randint(1,100) for _ in range(taille)]

def tri_selection(liste):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    n=len(liste)
    for i in range(n-1):
        min_index = i
        for j in range(i+1,n):
            if liste[j] < liste[min_index]:
                min_index = j
        liste[min_index],liste[j]=liste[j],liste[min_index]
        
TAILLE_MAX = 100
temps_mesure = []
for  t in range(1, TAILLE_MAX+1):
    temps = timeit.timeit(stmt = "tri_selection(liste_alea({}))".format(t),globals=globals(),number=5000)
    temps_mesure.append(temps)
plt.plot(range(1, TAILLE_MAX+1),temps_mesure)
plt.xlabel('longueurs des listes')
plt.ylabel('temps (secondes)')
plt.title('chronometrage du tri par selection pour liste_alea')
plt.show()
        