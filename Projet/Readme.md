
title : Projet image recursive  
author: M'bah Ange Pascal Tanoh


# journal
-06/03: importation des bibliotheques necessaires

-20/03: importation des bibliotheques necessaires pour manipuler les images
 20/03: definition de la fonction image_recursion : cette fonction prend en entrée une image et un ordre qui determine le niveau de detail de la représentation finale.
 elle effectue les etapes suivantes de manieres recursive:
 
 -si l'ordre est nul ou si l'image ou l'image est un pixel, la fonction retourne la couleur du pixel, sinon elle divise l'image en quatre blocs de taille egale.
 
 - elle appelle recursivement la fonction sur chaque bloc.
-
02/04: modification du projet du debut au commencement en commencant par la construction de la class Bloc et ses differentes methodes.

- ensuite je vais diviser l'image en 4 blocs

- ensuite je vais utiliser la methode qui va me permettre et renvoyer un tuple(r,g,b) representant la couleur moyenne d'un bloc

-La fonction modife_couleur modifie la couleur d'un bloc dans un programme Python. Elle parcourt les sous-blocs du bloc en question, et calcule la moyenne des valeurs rouge (r), vert (g) et bleu (b) pour tous les sous-blocs. Si la couleur d'un sous-bloc est un entier, il est considéré comme une valeur de couleur unique qui est ajoutée aux valeurs r, g ou b correspondantes. Si la couleur d'un sous-bloc est un tuple, il est considéré comme contenant trois valeurs représentant les valeurs r, g et b pour ce sous-bloc. La couleur finale du bloc est alors définie comme la moyenne des valeurs r, g et b de tous ses sous-blocs.

-La fonction est_uniforme est une méthode qui vérifie si un bloc est uniforme. Cette fonction renvoie une valeur booléenne, qui est True si le bloc n'a pas de sous-blocs et False dans le cas contraire.

-La fonction blocs_proches est une méthode  qui vérifie si deux blocs sont proches en fonction de leur couleur. Cette fonction prend deux paramètres, self et autre_bloc, qui représentent respectivement le bloc actuel et le bloc à comparer.

La fonction commence par vérifier si les deux blocs sont uniformes, c'est-à-dire s'ils ne contiennent pas de sous-blocs. Si l'un des deux blocs n'est pas uniforme, la fonction renvoie False pour indiquer qu'ils ne sont pas proches.

Si les deux blocs sont uniformes, la fonction calcule la différence entre les composantes rouge, verte et bleue de leur couleur respective. Elle utilise la fonction abs pour prendre la valeur absolue de chaque différence, et vérifie si chaque valeur est inférieure ou égale à 10.

Si toutes les valeurs absolues sont inférieures ou égales à 10, la fonction renvoie True pour indiquer que les deux blocs sont proches en termes de couleur. Sinon, la fonction renvoie False pour indiquer qu'ils ne sont pas proches.

-Cette fonction crée une nouvelle image en utilisant la bibliothèque . L'image créée a la même taille que le bloc spécifié dans les coordonnées self.bas_droit et self.haut_gauche.

Si le bloc est uniforme (c'est-à-dire si toutes les couleurs du bloc sont les mêmes), l'image sera remplie de la couleur spécifiée dans self.couleur.

Si le bloc n'est pas uniforme, l'image sera divisée en quatre sous-blocs égaux, et chaque sous-bloc sera rempli avec la couleur de son propre sous-bloc. Les sous-blocs sont créés en fonction des coordonnées self.sous_blocs, qui sont des instances de la même classe que l'objet actuel.

Enfin, l'image créée est retournée par la fonction.

-# Cette fonction est définie dans une classe et s'appelle "image_recursive". Elle prend un argument "ordre" en entrée:
Si l'ordre est supérieur à zéro :

    Diviser l'image en quatre sous-blocs en utilisant la méthode "diviser_en_quatre".
    Pour chaque sous-bloc, appeler récursivement la fonction "image_recursive" en passant l'ordre - 1.
    Vérifier si tous les sous-blocs sont proches en utilisant la méthode "blocs_proches".
    Si tous les sous-blocs sont proches, définir la couleur de l'image comme la couleur moyenne en utilisant la méthode "couleur_moyenne" et vider la liste "sous_blocs".
    Sinon, modifier la couleur de l'image en utilisant la méthode "modife_couleur".
    
et enfin voici l'explication pour manipuler l'image :
image = Image.open('calbuth.png') : Cette ligne ouvre l'image PNG nommée "calbuth.png" et l'assigne à une variable nommée image.

image_rgb = image.convert('RGB') : Cette ligne convertit l'image en mode de couleur RGB (Rouge, Vert, Bleu) et l'assigne à une variable nommée image_rgb.

bloc_initial = Bloc(image_rgb, (0, 0,0), (image_rgb.width, image_rgb.height)) : Cette ligne crée un nouvel objet Bloc avec l'image RGB en argument, ainsi que des arguments optionnels pour la couleur initiale (0, 0, 0) qui correspond au noir, et les dimensions de l'image (largeur et hauteur).

bloc_initial.image_recursive(1) : Cette ligne appelle la méthode image_recursive sur l'objet bloc_initial avec un argument de 1. Cette méthode semble être une fonction de segmentation d'image qui divise l'image en sous-blocs et répète le processus jusqu'à ce que tous les sous-blocs soient proches en termes de couleur.

image_finale = bloc_initial.creer_image() : Cette ligne crée une nouvelle image à partir de l'objet bloc_initial et l'assigne à une variable nommée image_finale.

image_finale.show() : Cette ligne affiche l'image finale dans une fenêtre.




# documentation
06/03: # importation des bibliotheques necessaires pou rmanipuler les images
20/03: # definition de la fonction image_recursion: def image_recursion(image, ordre):
       # diviser l'image en quatre blocs

02/04: # modification du projet du debut au commencement en commencant par la construction de la class Bloc et ses differentes methodes :

- #initialisation de la class Bloc et ses differentes methode
_#initialisation de la fonction init qui prend en parametre une image, haut_gauche, bas_droit, 
la couleur, et les sous_blocs comme l'ennonce l'indique

_ #utilisation de la methode de la class Bloc pour diviser l'image en quatre blocs

_#utilation de la methode de la class pour representer la couleur moyenne d'un bloc 
(tuple(r,g,b))

_ # utilisation de la methode qui va me permettre de modifier la couleur d'un bloc

_# utilisation de la methode est_uniforme qui verifie si un bloc est uniforme

_# utilisation de la methode blocs_proches qui verifie si deux blocs sont proches

_#utilisation de la methode creer_image pour creer une image de la meme taille que le blocs

-# Creation de l'algorithme nommé image recursive qui prend en parametre un ordre et self
-# quelque fonctionnalite pour manipuler l'image

