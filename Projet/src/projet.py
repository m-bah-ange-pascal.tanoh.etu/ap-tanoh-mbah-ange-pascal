from PIL import Image, ImageDraw

# Creation de la classe Bloc et ses differentes methodes

class Bloc:
    """Paramètres :
    à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
# initialisation de la fonction init
    def __init__(self, Image, haut_gauche, bas_droit, couleur=None, sous_blocs=None):
        """initialise un élement de type Bloc

        Précondition :
        Exemple(s) :
        $$$

        """
        
        self.image = Image
        self.haut_gauche = haut_gauche
        self.bas_droit = bas_droit
        self.couleur = couleur
        self.sous_blocs = sous_blocs or []
        
  # diviser l'image en quatre bloc      
    def diviser_en_quatre(self):
        """ Methode qui renvoie une liste de quatre objets Bloc

        Précondition :
        Exemple(s) :
        $$$

        """
        
        abscisse_x = (self.haut_gauche[0] + self.bas_droit[0]) // 2
        ordonne_y = (self.haut_gauche[1] + self.bas_droit[1]) // 2
        return [
            Bloc(self.image, self.haut_gauche, (abscisse_x, ordonne_y)),
            Bloc(self.image, (self.haut_gauche[0], ordonne_y), (abscisse_x, self.bas_droit[1])),
            Bloc(self.image, (abscisse_x, self.haut_gauche[1]), (self.bas_droit[0], ordonne_y)),
            Bloc(self.image, (abscisse_x, ordonne_y), self.bas_droit)
        ]
    
# Methode qui renvoie un tuple(r,g,b) representant la couleur moyenne d'un bloc     
    def couleur_moyenne(self):
        """Methode qui renvoie un tuple(r,g,b) representant la couleur moyenne d'un bloc

        Précondition : aucune
        Exemple(s) :
        $$$

        """
        
        couleur_r = 0
        couleur_g = 0
        couleur_b = 0
        total_pixels = 0
        for x in range(self.haut_gauche[0], self.bas_droit[0]):
            for y in range(self.haut_gauche[1], self.bas_droit[1]):
                pixel = self.image.getpixel((x, y))
                if isinstance(pixel, int):
                    couleur_r = couleur_r + pixel
                    couleur_g = couleur_g + pixel
                    couleur_b  = couleur_b + pixel
                else:
                    couleur_r  = couleur_r +  pixel[0]
                    couleur_g  = couleur_g + pixel[1]
                    couleur_b = couleur_b + pixel[2]
                total_pixels  = total_pixels +1
        return (couleur_r // total_pixels, couleur_g // total_pixels, couleur_b // total_pixels)

# methode qui va me permettre de modifier la couleur d'un bloc.  
    def modife_couleur(self):
            """Methode qui modifie la couleur d'un bloc  

            Précondition :
            Exemple(s) :
            $$$

            """
        
            if self.est_uniforme():
                return
            r = 0
            g = 0
            b = 0
            for bloc in self.sous_blocs:
                if isinstance(bloc.couleur, int):
                    r  = r + bloc.couleur
                    g  =  g + bloc.couleur
                    b = b+ bloc.couleur
                else:
                    r  = r + bloc.couleur[0]
                    g = g + bloc.couleur[1]
                    b = b + bloc.couleur[2]
            self.couleur = (r // 4, g // 4, b // 4)

# utilisation de la methode qui verifie si un bloc est uniforme

    def est_uniforme(self):
        """Methode qui verifie si un bloc est uniforme

        Précondition : aucune
        Exemple(s) :
        $$$

        """
        
        return len(self.sous_blocs) == 0
    
# utilisation de la methode qui verifie si deux blocs sont proches    
    def blocs_proches(self, autre_bloc):
        """Verifie si deux blocs sont proches

        Précondition : aucune
        Exemple(s) :
        $$$

        """
        
        if not self.est_uniforme() or not autre_bloc.est_uniforme():
            return False
        return abs(self.couleur[0] - autre_bloc.couleur[0]) <= 10 and \
               abs(self.couleur[1] - autre_bloc.couleur[1]) <= 10 and \
               abs(self.couleur[2] - autre_bloc.couleur[2]) <= 10
    
# utilisation de la fonction qui cree une nouvelle image de la meme tailleque le bloc
   
    def creer_image(self):
        """Crée une nouvelle image de la même taille que le bloc

        Précondition :
        Exemple(s) :
        $$$

        """
        image = Image.new('RGB', (self.bas_droit[0] - self.haut_gauche[0], self.bas_droit[1] - self.haut_gauche[1]))

        if self.est_uniforme():
            for x in range(image.width):
                for y in range(image.height):
                    image.putpixel((x, y), self.couleur)
        else:
            milieu_x = image.width // 2
            milieu_y = image.height // 2
            image.paste(self.sous_blocs[0].creer_image(), (0,0, milieu_x, milieu_y))
            image.paste(self.sous_blocs[1].creer_image(), (0, milieu_y, milieu_x, image.height))
            image.paste(self.sous_blocs[2].creer_image(), (milieu_x, 0, image.width, milieu_y))
            image.paste(self.sous_blocs[3].creer_image(), (milieu_x, milieu_y, image.width, image.height))

        return image

    
    def image_recursive(self, ordre):
        """Implémentation de l'algo

        Précondition :
        Exemple(s) :
        $$$

        """
        
        if ordre > 0:
            self.sous_blocs = self.diviser_en_quatre()
            for sous_bloc in self.sous_blocs:
                sous_bloc.image_recursive(ordre - 1)
            if all(self.blocs_proches(sous_bloc) for sous_bloc in self.sous_blocs):
                self.couleur = self.couleur_moyenne()
                self.sous_blocs = []
            else:
                self.modife_couleur()
        else:
            self.couleur = self.couleur_moyenne()
    
    
    
    
# manipulation de l'algo   
image = Image.open('calbuth.png')
image_rgb = image.convert('RGB')
bloc_initial = Bloc(image_rgb, (0, 0,0), (image_rgb.width, image_rgb.height))
bloc_initial.image_recursive(1)
image_finale = bloc_initial.creer_image()
image_finale.show()
