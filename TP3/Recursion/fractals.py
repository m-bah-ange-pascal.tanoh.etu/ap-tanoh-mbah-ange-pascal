#/TANOH/bin/python3 PASCAL
#31/01/2024
#TP3 GROUPE 15

import turtle

def zig_zag():
    """dessine un Zig zag

    Précondition : None
    Exemple(s) :
    $$$ 

    """
    for i in range(5):
        turtle.left(45)
        turtle.forward(40)
        turtle.right(90)
        turtle.forward(40)
        turtle.left(45)
    
def vonkoch(l:int|float, n:int):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    turtle.speed("fast")
    if n == 0:
        turtle.forward(l)
    else:
        vonkoch(l/3, n-1)
        turtle.left(60)
        vonkoch(l/3, n-1)
        turtle.right(120)
        vonkoch(l/3, n-1)
        turtle.left(120)
        vonkoch(l/3, n-1)
        return turtle