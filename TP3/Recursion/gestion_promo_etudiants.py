from etudiant import Etudiant
from date import Date
#1 Préliminaire

def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous([True, True, True])
    True
    $$$ pour_tous([True, False, True])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==True:
        i=i+1
    return i== len(seq_bool)

def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True ssi seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe([False, True, False])
    True
    $$$ il_existe([False, False])
    False
    """
    i=0
    while i<len(seq_bool) and seq_bool[i]==False:
        i=i+1
    if i<len(seq_bool):
        res=True
    else:
        res=False
    return res


def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res


L_ETUDIANTS = charge_fichier_etudiants("etudiants.csv")
COURTE_LISTE = L_ETUDIANTS[:10]

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    return all(isinstance(i,Etudiant) for i in x)

#Gestion de la promotion

#Q4-NBRE_ETUDIANTS=603
#Q5-NIP=42325167
#l'étudiant se trouvant s'appelle Le Roux Céline
#Q6-
L_moins_vingt_ans=[L_ETUDIANTS[i] for i in range(0,len(L_ETUDIANTS)) if Date(2,2,2004)<L_ETUDIANTS[i].naissance]

















































