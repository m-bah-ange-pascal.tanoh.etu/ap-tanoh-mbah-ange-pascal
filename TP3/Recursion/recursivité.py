#/TANOH/bin/python3 PASCAL
#31/01/2024
#TP3 GROUPE 15
def somme_de_deux_nombres(a:int, b:int)->int:
   """ Renvoie la sommes des entiers                                                                               

    Précondition : aucune
    Exemple(s) :
    $$$ somme_de_deux_nombres(100,200)
    300
    $$$ somme_de_deux_nombres(250,50)
    300
    """
    if a == 0 :
        res = b
    else:
        res = somme_de_deux_nombres(a-1, b+1)
    return res

def binomial(n:int, p:int)->float:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : n>=p
    Exemple(s) :
    $$$ binomial(5,2)
    10
    $$$ binomial(10,6)
    210
    
    """
    res = 0
    if p == 0  or n==p:
        res = 1
    else:
        res = binomial(n-1,p) + binomial(n-1, p-1)
    return res
    
def is_palindromic(mot:str)->bool:
        """à_remplacer_par_ce_que_fait_la_fonction

        Précondition : Aucune
        Exemple(s) :
        $$$ is_palindromic('assa')
        True
        $$$ is_palindromic('papa')
        False

        """
        mot = mot.lower()
        if 0<=len(mot)<=1:
            res = True
        else :
            res = mot[0] == mot[-1] and is_palindromic(mot[1:-1])
        return res
    
    