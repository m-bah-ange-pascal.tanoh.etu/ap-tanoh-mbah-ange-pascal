#/TANOH/bin/python3 PASCAL
#31/01/2024
#TP3 GROUPE 15

def fibo(n:int)->int:
    """renvoie le terme de la suite de Fibonacci.

    Précondition : 
    Exemple(s) :
    $$$ fibo(10)
    55
    $$$ fibo(4)
    3
    

    """
    
    res = 0
    if n == 0:
        res = 0
    elif 0 < n <=2:
        res = res + 1
    else:
        res = fibo(n-1) + fibo(n-2)
    return res