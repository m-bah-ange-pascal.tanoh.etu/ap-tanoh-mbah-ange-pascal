#TANOH M'BAH ANGE PASCAL
#17/01/2024
#GROUPE 15


s="la méthode split est parfois bien utile"

#s.split(' ')=['la', 'méthode','split','est','parfois','bien','utile']
#s.split('e')=['laméthod',' split ','st ',' parfois bi'n util', '']

#s.split('é')=['la m','thode split est parfois bien utile']

#s.split()=['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split('')=Valueerror,chaine vide ne doit pas être vide

#s.split('split')=['la méthode ', ' est parfois bien utile']

#2.La méthode split sépare la chaine de caractère en supprimant ce qui est dans les parenthèses
#3. Non , s n'est pas modifié , elle renvoie seulement une liste de la chaine séparé par la chaine de caractère dans les parenthèses.
l = s.split()
#Méthode sort
#"".join(l)='laméthodesplitestparfoisbienutile'

#" ".join(l)='la méthode split est parfois bien utile'
#";".join(l)='la;méthode;split;est;parfois;bien;utile'

#" tralala ".join(l)='la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'

#print ("\n".join(l))

#la
#méthode
#split
#est
#parfois
#bien
#utile

#"".join(s)='la méthode split est parfois bien utile'

#"!".join(s)='l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
#"".join()=Typeerror
#dans la parenthèse il doit y avoir une variable


#"".join([])=''
#"".join([1,2])=Typeerror
#Join accepte seulement les chaines de caractères
#2. La méthode join rassemble les chaines de caractères en fonction du caractère dans les parenthèses.
#3. Cette méthode ne modifie pas la chaine de caractère s. 

l = list('timoleon')

#l.sort()
#l=['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']
s1="Je n'ai jamais joué de flûte."
l1=list(s1)
#l1=['J', 'e', ' ', 'n', "'", 'a', 'i', ' ', 'j', 'a', 'm', 'a', 'i', 's', ' ', 'j', 'o', 'u', 'é', ' ', 'd', 'e', ' ', 'f', 'l', 'û', 't', 'e', '.']
#1. La méthode sort renvoie la liste de la chaine séparté caractères par caractère en les classant avec l'ordre léxicographique
l=['a',1]
#2.l.sort()=Typeerror, ne supporte pas les listes des entiers et chaines de caractères. La méthode sort() accepte les listes de str tout seul ou listes de int tout seul.
def sort(s: str) -> str:
    """
    Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.

    Précondition: aucune

    Exemples:

    $$$ sort('timoleon')
    'eilmnoot'
    """
    res=''
    l=list(s)
    l.sort()
    for elt in range(len(l)):
        res+=l[elt]
        
    return res

def sont_anagrammes1(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    True
    """
    i=0
    while i<len(s1) and i<len(s2) and s1[i]!=s2[i]:
        if len(s1)==len(s2):
            i+=1
    return s1[i]==s2[i]


def sont_anagrammes(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes('orange', 'organe')
    True
    $$$ sont_anagrammes('orange','Organe')
    False
    """

    
    l1=list(s1)
    l1.sort()
    l2=list(s2)
    l2.sort()
    return l1==l2 

def sont_anagrammes2(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False
    """
    
    occurrence_s1={}
    occurrence_s2={}
    for c in s1:
        #occurrence_s1[c]=occurrence_s1.get(c)+1
        occurrence_s1[c]={c}
    for c in s2: 
        #occurrence_s2[c]=occurrence_s2.get(c)+1
        occurrence_s2[c]={c}
    return occurrence_s1==occurrence_s2

def sont_anagrammes3(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    True
    """
    
    
    
    for c in s1:
        if s1.count(c)!=s2.count(c):
            res=False
        else:
            res=True
    
    return res
                

    
EQUIV_NON_ACCENTUE={'e':['è','é','ê'],'a':['à','â'],'i':['î'],'u':['ù']}

def bas_casse_sans_accent(chaine:str)->str:
    """ renvoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué

    Précondition : aucune
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
 

    """
    res=''
    for car in chaine:
        identique=False
        for cle,val in EQUIV_NON_ACCENTUE.items():
            if car in val:
                res+=cle.lower()
                identique=True
        if not identique:
            res+=car.lower()
    return res
            
                 
            
def sont_anagrammes4(s1: str, s2: str) -> bool:
    """
    Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition: aucune

    Exemples:

    $$$ sont_anagrammes4('orange', 'organe')
    True
    $$$ sont_anagrammes4('orange','Organe')
    True
    """
    
    occurrence_s1={}
    occurrence_s2={}
    for c in s1:
        mini=c.lower()
        
        occurrence_s1[mini]=occurrence_s1.get(mini)
    for c in s2:
        mini=c.lower()
        occurrence_s2[mini]=occurrence_s2.get(mini)
        
    return occurrence_s1==occurrence_s2
            
from lexique import LEXIQUE
#1.len(LEXIQUE)= 139719
#2. set(LEXIQUE)=5001

def anagrammes_v1(mot: str) -> list[str]:
    """
    Renvoie la liste des mots figurant dans le LEXIQUE qui sont des anagrammes de mot.

    Précondition: aucune

    Exemples:

    $$$ anagrammes_v1('orange')
    ['onagre', 'orange', 'orangé', 'organe', 'rongea']
    $$$ anagrammes_v1('info')
    ['foin']
    $$$ anagrammes_v1('Calbuth')
    []
    """
    liste=[]
    for elt in LEXIQUE:
        if sont_anagrammes(mot,elt):
            liste.append(elt)
    return liste
            
#Anagrammes d'un mot:seconde méthode
#Il y a beaucoup de clés à créer, et la recherche d'un mot spécifique pour trouver la liste de ses anagrammes implique de connaître toutes les clées. 

def cle(mot:str)->str:
    """ Fonction qui calcule la clé que doit avoir un mot donné dans le dictionnaire

    Précondition : 
    Exemple(s) :
    $$$ cle('Orangé')
    'aegnor'


    """
    correspond={'é': 'e', 'è': 'e', 'ê': 'e', 'à': 'a', 'â': 'a', 'î': 'i', 'ô': 'o', 'û': 'u'}
    mot_accent = ''.join(correspond.get(car,car) for car in mot.lower())
    mot_trie = ''.join(sorted(list(mot_accent)))
    return mot_trie
    
    
def ANAGRAMMES(lexique:list[str])->dict:
    """Construisez le dictionnaire des anagrammes, dont chaque association a pour clé la clé d'un mot du lexique, et pour valeur la liste des anagrammes

    Précondition : 
    Exemple(s) :
    

    """
    dictionnaire={}
    for mot_lexique in lexique:
        anagramme=cle(mot_lexique)
        if anagramme not in dictionnaire:
            dictionnaire[anagramme]=[]
        dictionnaire[anagramme].append(mot_lexique)
    print(dictionnaire)
    
    

