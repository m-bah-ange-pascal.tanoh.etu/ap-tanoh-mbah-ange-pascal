#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`war` game

:author: `FIL - Faculté des Sciences et Technologies -
         Univ. Lille <http://portail.fil.univ-lille1.fr>`

:date: 2021, april.
:last revision: 2024, march.
"""

from card import Card
from apqueue import *
from apstack import *
import random

def distribute(n_card: int) -> tuple[ApQueue, ApQueue]:
    """
    renvoie un couple (m1, m2) constitué de deux files,
    contenant pour chacune `n_card` cartes

    precondition : n_card > 0
    exemples :

    $$$ m1, m2 = distribute( 4 )
    $$$ len(m1) == 4
    True
    $$$ len(m2) == 4
    True
    $$$ type(m1) == ApQueue
    True
    $$$ type(m2) == ApQueue
    True
    $$$ carte = m1.dequeue()
    $$$ isinstance(carte, Card)
    True
    """
    m1 = ApQueue()
    m2 = ApQueue()
    liste_carte = Card.deck(n_card)
    
    for elt in range(0, len(liste_carte)):
        m1.enqueue(liste_carte[elt])
    for elt in range(0, len(liste_carte)):
        m2.enqueue(liste_carte[elt])
    return m1,m2
    
    
def gather_stack(main: ApQueue, pile: ApStack) -> None:
    """
    $$$ cartes = Card.deck(4)
    $$$ main = ApQueue()
    $$$ pile = ApStack()
    $$$ for c in cartes: pile.push(c)
    $$$ gather_stack( main, pile )
    $$$ len( main ) == 4
    True
    $$$ all( main.dequeue() == cartes[ 3 - i ] for i in range(3))
    True
    """
    while not pile.is_empty():
        main.enqueue(pile.pop())

def play_one_round(m1: ApQueue, m2: ApQueue, pile: ApStack) -> None:
    """
    Simule une étape du jeu :
    `j1`` et ``j2`` prennent la première carte de leur
    main. On compare les deux cartes :

    * Si la carte de ``j1`` est supérieure à celle de ``j2``, alors
    ``j1`` remporte toutes les cartes de la pile ;
    * Si la carte de ``j1`` est inférieure à celle de ``j2``, alors
    ``j2`` remporte toutes les cartes de la pile ;
    * Si les cartes sont égales, alors elles sont *empilées* sur la
      pile.

    precondition : m1 et m2 ne sont pas vides
    """
    card1 = m1.dequeue()
    card2 = m2.dequeue()
    print(f"joueur 1 joue {card1} et joueur 2 joue {card2}")
    if card1.compare(card2) == 1:
        print("joueur 1 gagne")
        gather_stack(m1, pile)
    elif card1.compare(card2) == -1:
        print("joueur 2 gagne")
        gather_stack(m2, pile)
    else:
        print("Bataille !!")
        pile.push(card1)
        pile.push(card2)


def play(n_card: int, n_round: int) -> None:
    """
    simule une partie de bataille

    n_card: le nombre de cartes à distribuer à chaque joueur.
    n_round: le nombre maximal de tours
    """
    m1, m2 = distribute(n_card)
    pile = ApStack()
    for i in range(n_round):
        print(f"------ Tour {i+1} --------")
        play_one_round(m1, m2, pile)
        print(f"Le joueur 1 a {len(m1)} cartes et le joueur 2 a {len(m2)} cartes")
        if m1.is_empty() or m2.is_empty():
            break
    if len(m1) > len(m2):
        print("Le joueur 1 a gagné")
    elif len(m1) < len(m2):
        print("Le joueur 2 a gagné")
    else:
        print("Égalité")


if __name__ == "__main__":
    import apl1test
    apl1test.testmod("war.py")

