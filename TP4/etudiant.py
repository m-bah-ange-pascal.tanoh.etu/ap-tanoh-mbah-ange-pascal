#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:author: FIL - FST - Univ. Lille.fr <http://portail.fil.univ-lille.fr>_
:date: janvier 2019
:last revised: 
:Fournit :
"""
from date import Date


class Etudiant:
    """
    une classe représentant des étudiants.

    $$$ etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ str(etu)
    'Tim Oléon'
    $$$ repr(etu)
    '314159 : Tim OLÉON'
    $$$ etu.prenom
    'Tim'
    $$$ etu.nip
    314159
    $$$ etu.nom
    'Oléon'
    $$$ etu.formation
    'MI'
    $$$ etu.groupe
    '15'
    $$$ etu2 = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu2
    True
    $$$ etu3 = Etudiant(141442, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu3
    False
    $$$ etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')
    $$$ etu < etu4
    True
    $$$ isinstance(etu.naissance, Date)
    True
    """
    def __init__(self, nip: int, nom: str, prenom: str,
                 naissance: Date, formation: str, groupe: str):
        """
        initialise un nouvel étudiant à partir de son nip, son nom, son
        prénom, sa formation et son groupe.

        précondition : le nip, le nom et le prénom ne peuvent être nuls ou vides.
        """
        ...

    def __eq__(self, other) -> bool:
        """
        Renvoie True ssi other est un étudiant ayant :
        - même nip,
        - même nom et
        - même prénom que `self`,
        et False sinon.
        """
        ...

    def __lt__(self, other) -> bool:
        """
        Renvoie True si self est né avant other
        """
        ...

    def __str__(self) -> str:
        """
        Renvoie une représentation textuelle de self.
        """
        ...

    def __repr__(self) -> str:
        """
        Renvoie une représentation textuelle interne de self pour le shell.
        """
        ...

if (__name__ == "__main__"):
    import apl1test
    apl1test.testmod('etudiant.py')
